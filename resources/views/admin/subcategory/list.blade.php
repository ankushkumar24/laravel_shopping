@extends('admin/admin_layout.admin_layout')

@section('page_title','CATEGORY SUBCATEGORY')

@section('container')

@section('list_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- <script src="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
@endsection

<div class="">
	<div class="page-title">
		<div class="title_left">
			<h4>SUBCATEGORY</h4>
			  	<a href="/admin/subcategory/subcategory" class="btn btn-info">Add SUBCATEGORY</a></button>
		</div>
	</div>
	<div class="clearfix"></div>
	    <div class="row">
	       	<div class="col-md-12 col-sm-12 " style="color:red;text-align: center;height: 100;width: 100">
	        	<!-- <div style="background-color: yellow;height: 100px;width:100%">
	        		<span style="color: black"><h1>Welcome {{session('BLOG_USER_NAME')}}</h1></span>
	        	</div>
	        	<div style="background-color: yellow;height: 50px;width:100%">
	        		<span style="color: black"><h3>{{session('msg')}}</h3></span>
	        	</div> -->
	  	</div>
		<div class="col-md-12 col-sm-12 ">
			<div class="x_panel">
			   <div class="x_content">
				  <div class="row">
					 <div class="col-sm-12">
						<div class="card-box table-responsive">
						   <table id="datatable" class="table table-striped table-bordered" style="width:100%">
							  <thead>
								<tr>
									<th width="2%">S.No</th>
									<th width="10%">category_id</th>
									<th width="30%">subcategory_name</th>
                                     <th width="30%">subcategory_status</th>
									<th width="20%">Action</th>
								</tr>
							</thead>
							<tbody>
						    @foreach($subcategory as $subcat)
								<tr>
								 	<input type="hidden" class="remove"  value="" >
									<td>{{$subcat->id}}</td>
									<td>{{$subcat->category_id}}</td>
									<td>{{$subcat->subcategory_name}}</td>
                                    <td>{{$subcat->subcategory_status}}</td>
									
								<td>

								<a href="{{url('edit/'.$subcat->id)}}" class="btn btn-info">EDIT</a>

								<a href="{{url('listingsub/'.$subcat->id)}}"  class="btn btn-danger remove">DELETE</a>
                                        	 
								</td>
								</tr>

							@endforeach
                                         
							    </tbody>
						   </table>
                              <div class="d-flex justify-content-center">
                              	{{ $subcategory->links() }}
                              
                              </div>
						</div>
					 </div>
				  </div>
			   </div>
			</div>
		 </div>
	  </div>
   </div>
 <!-- <script>

 	$('.remove').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    swal({
        title: 'Are you sure?',
        text: 'This record and it`s details will be permanantly deleted!',
        icon: 'warning',
        buttons: ["Cancel", "Yes!"],
    }).then(function(value) {
        if (value) {
            window.location.href = url;
        }
    });
});
</script> -->

@endsection