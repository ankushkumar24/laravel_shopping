@extends('admin/admin_layout/admin_layout')

@section('page_title','ADD SUBCATEGORY')

@section('container')

   <div class="">
          <div class="page-title">
               <div class="title_left">
                  <h3>ADD  SUBCATEGORY</h3>
                  <a href="/listingsub" class="btn btn-success">BACK TO SUBCATEGORY LIST</a></button>

               </div>
          </div>
            <div class="clearfix"></div>
               <div class="row">
                 <div class="col-md-12 ">
                    <div class="x_panel">
                      <div class="x_content">
                         <br/>

            <form class="form-horizontal form-label-left" method="post" action="{{url('/admin/SubController/insert')}}"  >

            @csrf
            <div class="form-group row ">
                <label class="control-label col-md-3 col-sm-3 ">category_id*</label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" placeholder="category_id" value="" name="category_id">
                        @error('category_id')
                               <span  style="color:red;" class="field_error">{{$message}}</span> 
                        @enderror
                        <label> The category_id field is required</label>
                   </div>
            </div>
              

            <div class="form-group row ">
                <label class="control-label col-md-3 col-sm-3 ">subcategory_name*</label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" placeholder="subcategory_name" value="" name="subcategory_name">
                        @error('subcategory_name')
                               <span  style="color:red;" class="field_error">{{$message}}</span> 
                        @enderror
                        <label> The subcategory_name field is required</label>
                </div>
            </div>
              
            <div class="form-group row ">
                <label class="control-label col-md-3 col-sm-3 ">subcategory_status*</label>
                    <div class="col-md-9 col-sm-9 ">
                    <input type="text" class="form-control" placeholder="subcategory_status" value="" name="subcategory_status"> 
                        @error('subcategory_name')
                               <span  style="color:red;" class="field_error">{{$message}}</span> 
                        @enderror 
                        <label> The subcategory_status<span style="color: green">"NOT"</span> field is required</label>         
                </div>                     
            </div>


            <div class="ln_solid">
                <div class="form-group">
                    <div class="col-md-9 col-sm-9  offset-md-3">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </div>             
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection