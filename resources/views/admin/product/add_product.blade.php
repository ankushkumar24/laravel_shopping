@extends('admin/admin_layout/admin_layout')

@section('page_title','ADD PRODUCT')

@section('container')

   <div class="">
          <div class="page-title">
               <div class="title_left">
                  <h3>ADD PRODUCT</h3>
                  <a href="/list_get" class="btn btn-success">BACK TO PRODUCT LIST</a></button>

               </div>
          </div>
            <div class="clearfix"></div>
               <div class="row">
                 <div class="col-md-12 ">
                    <div class="x_panel">
                      <div class="x_content">
                      <br/>

            <form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data" action="{{url('/admin/StockController/product_insert')}}"   >
              
                @csrf
                <div class="form-group row ">
                 <label class="control-label col-md-3 col-sm-3 ">subcategory_id*</label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" placeholder="subcategory_id" value="{{old('subcategory_id')}}" name="subcategory_id">
                        @error('subcategory_id')
                               <span  style="color:red;" class="field_error">{{$message}}</span> 
                        @enderror
                        <label> The subcategory_id field is required</label>

                   </div>
                </div>
              
                <div class="form-group row ">
                  <label class="control-label col-md-3 col-sm-3 ">product_name*</label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" placeholder="product_name" value="{{old('product_name')}}" name="product_name">
                        @error('product_name')
                               <span  style="color:red;" class="field_error">{{$message}}</span> 
                        @enderror
                        <label> The product_name field is required</label>

                    </div>                     
                </div>
                <div class="form-group row ">
                  <label class="control-label col-md-3 col-sm-3 ">prizes*</label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" placeholder="prizes" value="{{old('prizes')}}" name="prizes">
                        @error('prizes')
                               <span  style="color:red;" class="field_error">{{$message}}</span> 
                        @enderror
                        <label> The prizes field is required</label>

                    </div>                     
                </div>
                <div class="form-group row ">
                  <label class="control-label col-md-3 col-sm-3 ">image*</label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="file" class="form-control" placeholder="image" value="{{old('image')}}" name="image">
                        @error('image')
                               <span  style="color:red;" class="field_error">{{$message}}</span> 
                        @enderror
                        <label> The image field is required</label>

                    </div>                     
                </div>
                <div class="form-group row ">
                  <label class="control-label col-md-3 col-sm-3 ">product_status*</label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" placeholder="product_status" value="{{old('product_status')}}" name="product_status">
                        @error('product_status')
                               <span  style="color:red;" class="field_error">{{$message}}</span> 
                        @enderror
                        <label> The product_status<span style="color: green">"NOT"</span> field is required</label>

                    </div>                     
                </div>


                <div class="ln_solid"></div>
                    <div class="form-group">
                    <div class="col-md-9 col-sm-9  offset-md-3">
                        <button type="submit"  class="btn btn-success">Submit</button>
                    </div>
                </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
@endsection