@extends('admin/admin_layout/admin_layout')

@section('page_title','EDIT CATEGORY')

@section('container')

   <div class="">
          <div class="page-title">
               <div class="title_left">
                  <h3>EDIT CATEGORY</h3>

               </div>
          </div>
            <div class="clearfix"></div>
               <div class="row">
                 <div class="col-md-12 ">
                    <div class="x_panel">
                      <div class="x_content">
                      <br/>

            <form class="form-horizontal form-label-left" method="post" action="{{url('modify/'.$category->first()->id)}}">
              
                @csrf
            <div class="form-group row ">
                <label class="control-label col-md-3 col-sm-3 ">category_name*</label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" placeholder="category_name" value="{{$category->first()->category_name}}" name="category_name">

                        @error('category_name')
                               <span  style="color:red;" class="field_error">{{$message}}</span> 
                        @enderror

                    </div>
            </div>
              
            <div class="form-group row ">
                <label class="control-label col-md-3 col-sm-3 ">category_status*</label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" placeholder="category_status" value="{{$category->first()->category_status}}" name="category_status">

                        @error('category_status')
                               <span  style="color:red;" class="field_error">{{$message}}</span> 
                        @enderror

                    </div>                     
            </div>


            <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-9 col-sm-9  offset-md-3">
                        <button type="submit"  class="btn btn-success">Submit</button>
                </div>
            </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
@endsection