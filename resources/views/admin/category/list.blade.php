@extends('admin/admin_layout.admin_layout')

@section('page_title','CATEGORY LISTING')

@section('container')

@section('list_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection

<div class="">
	<div class="page-title">
		<div class="title_left">
			<h4>LIST OF CATEGORY</h4>
			<a href="/admin/category/add_category" class="btn btn-danger">LIST OF CATEGORY</a></button>
		</div>
	</div>
	<div class="clearfix"></div>
	      <div class="row">
	       	<div class="col-md-12 col-sm-12 " style="color:red;text-align: center;height: 100;width: 100">
	        	<!-- <div style="background-color: yellow;height: 100px;width:100%">
	        		<span style="color: black"><h1>Welcome {{session('BLOG_USER_NAME')}}</h1></span>
	        	</div>
	        	<div style="background-color: yellow;height: 50px;width:100%">
	        		<span style="color: black"><h3>{{session('msg')}}</h3></span>
	        	</div> -->
	  	</div>
		<div class="col-md-12 col-sm-12 ">
			<div class="x_panel">
			   <div class="x_content">
				  <div class="row">
					 <div class="col-sm-12">
						<div class="card-box table-responsive">
						   <table id="datatable" class="table table-striped table-bordered" style="width:100%">
							  <thead>
								<tr>
									<th width="2%">S.No</th>
									<th width="10%">category_name</th>
									<th width="30%">category_status</th>
									<th width="20%">Action</th>
								</tr>
							</thead>
							<tbody>
						    @foreach($category as $cat)
								 <tr>
								 	<input type="hidden" class="remove"  value="" >
									<td>{{$cat->id}}</td>
									<td>{{$cat->category_name}}</td>
									<td>{{$cat->category_status}}</td>	
								<td>
						    	<a href="{{url('edited/'.$cat->id)}}" class="btn btn-info">EDIT</a>
						    	<a href="{{url('listingdelete/'.$cat->id)}}"  class="btn btn-danger remove">DELETE</a>       	 
								</td>
								</tr>
                            @endforeach
							</tbody>
						    </table>
                            <div class="d-flex justify-content-center">
                            	 {{ $category->links() }}
                              
                            </div>
						</div>
					 </div>
				  </div>
			   </div>
			</div>
		 </div>
	  </div>
   </div>
 <!-- <script>

 	$('.remove').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    swal({
        title: 'Are you sure?',
        text: 'This record and it`s details will be permanantly deleted!',
        icon: 'warning',
        buttons: ["Cancel", "Yes!"],
    }).then(function(value) {
        if (value) {
            window.location.href = url;
        }
    });
});
</script> -->

@endsection