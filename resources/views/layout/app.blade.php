<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>StayHome.store</title>
        <link rel="icon" href="assets/imgs/favicon.ico" sizes="32x32">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <!-- Custom Stylesheet -->
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>
        
        <!-- Begin Header -->
        <div class="navbar-fixed-top bg-white">
            <!-- Begin Header -->
            <header class="navbar navbar-default custom-header" role="navigation">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-4">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand dev-logo" href="#"><img src="assets/imgs/stay-home-logo.png"></a>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-1">
                        </div>
                        <div class="col-md-6 col-sm-5">
                            <form class="navbar-left search-bar w-100" role="search">
                                <div class="search">
                                    <input type="text" class="searchTerm" placeholder="Search">
                                    <button type="submit" class="searchButton">
                                        <img src="assets/imgs/Icon awesome-search.png">
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="cart-btn">
                                    <a href="#" data-toggle="modal" data-target="#cartModal">
                                        <img src="assets/imgs/cart.png">
                                        Cart
                                    </a>
                                </li>
                            </ul>
                            <nav>

                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                </div>
                <!-- /container-->
            </header>
            <!-- End Header -->

            <!-- Begin Navigation -->
            <nav class="navbar main-navbar">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse js-navbar-collapse mega-drop">
                    <div class="container">
                        <ul class="nav navbar-nav">
                            <li class="dropdown mega-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="nav-img"><img src="assets/imgs/toggle.png"></span> Departments</a>              
                                <ul class="dropdown-menu mega-dropdown-menu">
                                    <div class="container drop-container">
                                        <div class="row">
                                            <li class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/vegies.jpg" alt="...">
                                                                <div class="caption">
                                                                    <h5>Produce</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/dairy-eggs-cat-1-300x300.jpg" alt="...">
                                                                <div class="caption">
                                                                    <h5>Dairy</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/37621138_l_clipped_rev_1-300x300.jpg" alt="...">
                                                                <div class="caption">
                                                                    <h5>Meat</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/fish.jpg" alt="...">
                                                                <div class="caption">
                                                                    <h5>Fish</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/Kedem-Grape-Juice-32-300x300.jpg" alt="...">
                                                                <div class="caption">
                                                                    <h5>Bevarages</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/siteImage.png" alt="...">
                                                                <div class="caption">
                                                                    <h5>Bevarages</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/candy-sweets-cat-300x300.jpg" alt="...">
                                                                <div class="caption">
                                                                    <h5>Candies</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/baby-cat-1-300x300.jpg" alt="...">
                                                                <div class="caption">
                                                                    <h5>Baby Care</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                </ul>               
                            </li>
                            <li class="dropdown mega-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="nav-img"><img src="assets/imgs/stores.png"></span>Stores</a>                
                                <ul class="dropdown-menu mega-dropdown-menu">
                                    <div class="container drop-container">
                                        <div class="row">
                                            <li class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/butcher_logo.jpg" alt="...">
                                                                <div class="caption">
                                                                    <h5>Crown Heights Butcher</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/costco_logo.jpg" alt="...">
                                                                <div class="caption">
                                                                    <h5>Costco</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/kettle_cord_logo.png" alt="...">
                                                                <div class="caption">
                                                                    <h5>Kettle & Cord</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/kleins.png" alt="...">
                                                                <div class="caption">
                                                                    <h5>Klein's</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/shunshine_logo.jpg" alt="...">
                                                                <div class="caption">
                                                                    <h5>Sunshine</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/treasure_shoes_logo.jpg" alt="...">
                                                                <div class="caption">
                                                                    <h5>Treasure Shoes</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/wine-by-the-case-logo-600x600.jpg" alt="...">
                                                                <div class="caption">
                                                                    <h5>Wine By The Case</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/zakons-logo-600x600.jpg" alt="...">
                                                                <div class="caption">
                                                                    <h5>Zakon's Toy Store</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <a href="#">
                                                            <div class="thumbnail border-0 text-center">
                                                                <img src="assets/imgs/1982-coffee-roasters-logo.jpg" alt="...">
                                                                <div class="caption">
                                                                    <h5>1982 Coffee Roasters</h5>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                </ul>              
                            </li>
                            <li class="dropdown mega-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="nav-img"><img src="assets/imgs/special.png"></span>Specials</a>              
                            </li>
                            <li class="dropdown mega-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="nav-img"><img src="assets/imgs/order.png"></span>Orders </a>                
                            </li>
                            <li class="dropdown mega-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="nav-img"><img src="assets/imgs/re-order.png"></span>Re-Order</a>                
                            </li>
                            <li class="dropdown ml-12">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="nav-img"><img src="assets/imgs/login.png"></span>Login</a> </li>
                        </ul>
                    </div>
                </div><!-- /.nav-collapse -->
            </nav>
            <!-- End Navigation -->
        </div>
        <!-- End Header -->

        <!--Begin Section -->
        <section class="section-bg custom-block section-one">
            <div class="container">
            @yield ('content')
            </div>
        </section>
        <!-- End Section -->

        <!-- Begin Section -->
        <section class="popular-items">
            <div class="store-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block-h2">
                                <h3>Popular Items</h3>
                            </div>
                            <div class="carousel carousel-showmanymoveone slide" id="itemslider1">
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                            <div class="store-block">
                                                <a href="#"><img src="assets/imgs/costco_logo.jpg" class="img-responsive center-block"></a>
                                                <h4 class="text-center">Costco</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                            <div class="store-block">
                                                <a href="#"><img src="assets/imgs/butcher_logo.jpg" class="img-responsive center-block"></a>
                                                <h4 class="text-center">Crown Heights Butcher</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                            <div class="store-block">
                                                <a href="#"><img src="assets/imgs/bentz_logo.jpg" class="img-responsive center-block"></a>
                                                <h4 class="text-center">Benz's Homemade Taste</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                            <div class="store-block">
                                                <a href="#"><img src="assets/imgs/shunshine_logo.jpg" class="img-responsive center-block"></a>
                                                <h4 class="text-center">Sunshine</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                            <div class="store-block">
                                                <a href="#"><img src="assets/imgs/wine-by-the-case-logo-600x600.jpg" class="img-responsive center-block"></a>
                                                <h4 class="text-center">Wine By The Case</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                            <div class="store-block">
                                                <a href="#"><img src="assets/imgs/zakons-logo-600x600.jpg" class="img-responsive center-block"></a>
                                                <h4 class="text-center">Zakon's Toy Store</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="col-xs-12 col-sm-6 col-md-2">
                                            <div class="store-block">
                                                <a href="#"><img src="assets/imgs/merkazStam-2.jpg" class="img-responsive center-block"></a>
                                                <h4 class="text-center">Merkaz Stam</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="slider-control">
                                    <a class="left carousel-control" href="#itemslider" data-slide="prev"><img src="assets/imgs/prev.png" alt="Left" class="img-responsive"></a>
                                    <a class="right carousel-control" href="#itemslider" data-slide="next"><img src="assets/imgs/next.png" alt="Right" class="img-responsive"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Section -->

        <!-- Begin Footer -->
        <footer class="grey-bg">
            <div class="container">
                <div class="footer-inner">
                    <div class="row">
                        <div class="col-md-4">
                            <ul class="ftr-list">
                                <li><img src="assets/imgs/stay-home-logo-white.png"></li>
                                <li>411 Troy Ave. Brooklyn, NY 11213</li>
                                <li>(347) 770-1710</li>
                                <li>orders@stayhome.store</li>
                                <li>© 2019 - 2020 Stayhome.store</li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <ul class="ftr-links">
                                <li>Return Policy</li>
                                <li>Terms & Conditions</li>
                                <li>Privacy Policy</li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <ul class="ftr-links">
                                <li>Your Account</li>
                                <li>Order History</li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul class="ftr-links">
                                <li>Contact</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->

        <!-- Begin Side Modal -->
        <div class="modal right fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="cartModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-right mr-2 border-0">
                        <buttom class="btn green-bg custom-btn">
                            Checkout
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row dark-bg">
                            <div class="col-md-12">
                                <table class="table cart-list">
                                    <tbody>
                                        <tr>
                                            <td><img src="assets/imgs/iconfinder_shopping-cart_1608412.png"></td>
                                            <td class="text-white">2 Items</td>
                                            <td class="text-right text-white"> <strong>$11.20</strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="items-bar">
                                    <div class="items-list">
                                        <div class="row row-bd mt-1">
                                            <div class="col-md-4 col-sm-4 pr-0">
                                                <span><img src="assets/imgs/255-e1569562891404-300x300.jpg" class="w-100 img-rounded"></span>
                                            </div>
                                            <div class="col-md-5 col-sm-5 pr-0">
                                                <div class="item-name">
                                                    Lior Medjool Dates With Pit,14.1 Oz
                                                </div>
                                                
                                                <span class="item-price">
                                                    <del>$5.99</del> $5.60 each
                                                </span>
                                                
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="text-right">
                                                    <div class="delete-item">
                                                        <img src="assets/imgs/cancel.png">
                                                    </div>
                                                    <span class="item-price"><strong>$5.60</strong></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row row-bd mt-1">
                                            <div class="col-md-4 col-sm-4 pr-0">
                                                <span><img src="assets/imgs/255-e1569562891404-300x300.jpg" class="w-100 img-rounded"></span>
                                            </div>
                                            <div class="col-md-5 col-sm-5 pr-0">
                                                <div class="item-name">
                                                    Lior Medjool Dates With Pit,14.1 Oz
                                                </div>
                                                
                                                <span class="item-price">
                                                    <del>$5.99</del> $5.60 each
                                                </span>
                                                
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="text-right">
                                                    <div class="delete-item">
                                                        <img src="assets/imgs/cancel.png">
                                                    </div>
                                                    <span class="item-price"><strong>$5.60</strong></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button class="btn green-bg custom-btn">
                                        Go To Checkout
                                    </button>
                                </div>
                            </buttom>
                        </div>
                    </div>
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        <!-- End Side Modal -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#itemslider', '#itemslider1').carousel({ interval: 3000 });

                $('.carousel-showmanymoveone .item').each(function(){
                    var itemToClone = $(this);

                    for (var i=1;i<6;i++) {
                        itemToClone = itemToClone.next();

                        if (!itemToClone.length) {
                            itemToClone = $(this).siblings(':first');
                        }

                        itemToClone.children(':first-child').clone()
                        .addClass("cloneditem-"+(i))
                        .appendTo($(this));
                    }
                });

                $(".dropdown").hover(            
                    function() {
                        $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
                        $(this).toggleClass('show');        
                    },
                    function() {
                        $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
                        $(this).toggleClass('show');       
                    }
                );

                $('.sidenav-toggle').on('click', function() {
                    var $sidenav, $this;
                    $this = $(this);
                    $sidenav = $('.ct-sidenav');
                    if ($this.hasClass('active')) {
                        $this.removeClass('active');
                        return $sidenav.removeClass('active');
                    } else {
                        $this.addClass('active');
                        return $sidenav.addClass('active');
                    }
                });
                $('#sidenav-toggle').on('click', function() {
                    var $sidenav, $this;
                    $this = $(this);
                    $sidenav = $('.ct-sidenav');
                    if ($this.hasClass('open')) {
                        $this.removeClass('open');
                        return $sidenav.removeClass('open');
                    } else {
                        $this.addClass('open');
                        return $sidenav.addClass('open');
                    }
                });
                $('.ct-sidenav').find('button.close').on('click', function() {
                    $(this).parent().removeClass('open');
                    return $('#sidenav-toggle').removeClass('open');
                });
            });
        </script>
    </body>
</html>