@extends('layout.app')
@section ('content') 

    <section class="section-bg section-one">
            <div class="store-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block-h2">
                                <h2>Products</h2>
                            </div>
                        </div>
                    </div>
                    <div class="product-category">
                        <div class="row">
                        <div class="container-fluid">
                          <div class="row" >
                            @foreach($product as $list)
                            <div class="col-md-3">
                                <a href="#">
                                    <div class="thumbnail product-card text-center">
                                     <a href=""><img src="{{url('$list->image')}}" height="212" width="212"></a>
                                     <h4>Name:{{$list->product_name}} </h4>
                                     <p>prize:{{$list->prizes}} </p>
                                    </div>
                                </a>
                            </div>
                            @endforeach 
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection



            