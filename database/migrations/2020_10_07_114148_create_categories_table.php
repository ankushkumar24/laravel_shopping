<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('category_name');
            $table->enum('category_status', array('enable', 'disable'))->default('enable');
            $table->timestamps();
        });
        Schema::create('subcategories', function (Blueprint $table) {
            $table->id();
            $table->string('category_id');
            $table->string('subcategory_name');
            $table->enum('subcategory_status', array('enable', 'disable'))->default('enable');
            $table->timestamps();
        });
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('subcategory_id');
            $table->string('product_name');
            $table->string('prizes');
            $table->string('image');
            $table->enum('product_status', array('enable', 'disable'))->default('enable');
            $table->timestamps();
        });
    }

   
    public function down()
    {
        Schema::dropIfExists('categories');
        Schema::dropIfExists('subcategories');
        Schema::dropIfExists('products');
    }
}
