<?php

use Illuminate\Support\Facades\Route;
use App\http\controllers\ShoppingController;
use App\http\controllers\ProductController;
use App\http\controllers\admin\AdminController;
use App\http\controllers\admin\SubController;
use App\http\controllers\admin\StockController;




Route::get('/', function () {
    
    return view('welcome');
});
/*front end route started*/

route::view('shopping','layout/app');
route::view('catgroy','catgory');
route::get('shop',[ProductController::class,'index']);

/*front end route ended*/

/*admin panel end route started*/
 route::view('admin/category/add_category','admin/category/add_category');
 route::post('/admin/AdminController/insert',[AdminController::class,'insert']);
 route::view('listingdelete','admin/category/list');
 route::get('listingdelete',[AdminController::class,'show']);
 route::get('listingdelete/{id}',[AdminController::class,'delete']);
 Route::view('admin/category/edit','admin/category/edit');
 Route::get('/edited/{id}',[AdminController::class,'edit']);
 Route::post('/modify/{id}',[AdminController::class,'modify']);



 route::view('admin/subcategory/subcategory','admin/subcategory/subcategory');
 route::post('/admin/SubController/insert',[SubController::class,'insert']);
 route::view('listingsub','admin/subcategory/list');
 route::get('listingsub',[SubController::class,'show']);
 route::get('listingsub/{id}',[SubController::class,'delete']);
 Route::view('admin/subcategory/edit','admin/subcategory/edit');
 Route::get('/edit/{id}',[SubController::class,'edit']);
 Route::post('/update/{id}',[SubController::class,'update']);
 
  
route::view('product','admin/product/add_product');
route::get('list_get',[StockController::class,'get_data']);
route::post('/admin/StockController/product_insert',[StockController::class,'product_insert']);
route::get('/list_get/{id}',[StockController::class,'delete']);
route::get('/edit_list/{id}',[StockController::class,'edit']);
Route::post('/update/{id}',[StockController::class,'update']);
 
route::view('/admin/logo/add_logo','admin/logo/add_logo');

/*admin panel end route ended*/