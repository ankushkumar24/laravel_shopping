<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{

    public $table=('subcategories');
    protected $fillable=['category_id','subcategory_name','subcategory_stutus'];
  
}
