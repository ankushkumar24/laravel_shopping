<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{


	public $table=('products');
	protected $fillable=['subcategory_id','product_name','prizes','image','product_status'];
    
}
