<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Category;

class AdminController extends Controller
{
    function index(){   
        return view('admin/category/add_category');
    }
    function show(){
        $category=DB::table('categories')->simplePaginate(2);
        return view('admin/category/list',compact('category'));
    }
    function insert(Request $request){

        $validatedData = $request->validate([
            'category_name' => 'required',
            
        ]);
        
        $submit=new Category();
        $submit->category_name=$request->input('category_name');
        $submit->save();
        return redirect('listingdelete');
        

    }
    function delete(request $request,$id){

        DB::table('categories')->where('id',$id)->delete();
        return redirect('listingdelete');
 
    }
    function edit($id){

        $category=DB::table('categories')->where('id',$id)->get();
        return view('admin/category/edit', compact('category'));
    }
    function modify(request $request,$id){
     
        $validatedData = $request->validate([

            'category_name' =>'required',
              
        ]);
        
        $sure= Category::findorfail($id);
        $sure->category_name=$request->input('category_name');
        $sure->save();
        return redirect('listingdelete');
    }
}
