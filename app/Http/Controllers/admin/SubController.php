<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\SubCategory;

class SubController extends Controller
{
   
    function show(){
        $subcategory=DB::table('subcategories')->simplePaginate(5);
        return view('admin/subcategory/list',compact('subcategory'));
    }
    function insert(Request $request){

        $validatedData = $request->validate([
        	'category_id'=>'required',
            'subcategory_name' => 'required',
            
            
        ]);
        
        $submit=new SubCategory();
        $submit->category_id=$request->input('category_id');
        $submit->subcategory_name=$request->input('subcategory_name');
        $submit->save();
        return redirect('listingsub');
        

   }
    function delete(request $request,$id){

    	DB::table('subcategories')->where('id',$id)->delete();
    	return redirect('listingsub');
 
    }
    function edit($id){

   	    $subcategory=DB::table('subcategories')->where('id',$id)->get();
   	    return view('admin/subcategory/edit',compact('subcategory'));
    }
    function update(request $request,$id){
        
        $validatedData = $request->validate([

            'category_id'=>'required',
            'subcategory_name' => 'required',
            
            
        ]);
        
        $submit= SubCategory::findorfail($id);
        $submit->category_id=$request->input('category_id');
        $submit->subcategory_name=$request->input('subcategory_name');
        $submit->save();
        return redirect('listingsub');

    }
}
