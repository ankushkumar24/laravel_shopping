<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Stock;


class StockController extends Controller
{
                function show(){
            
                	return view('admin/product/list');
                }
                function get_data(){
    
                    $stocks=DB::table('products')->simplePaginate(5);
                    return view('admin/product/list',compact('stocks'));
                }
                function product_insert(Request $request){
            
                    $validatedData = $request->validate([
                    'subcategory_id' => 'required',
                    'product_name' => 'required',
                    'prizes' => 'required',
                    'image' => 'required',
                    'product_status' => 'required',
                    
                ]);

             	$insert=new Stock();
             	$insert->subcategory_id=$request->input('subcategory_id');
             	$insert->product_name=$request->input('product_name');
             	$insert->prizes=$request->input('prizes');
                $insert->image=$request->file('image');
             	$insert->product_status=$request->input('product_status');

                if($request->hasfile('image')){
                $image=$request->file('image');
                $image_name = $request->image->getClientOriginalName();
                $image->storeAs('/public/post',$image_name); 
                $insert['image']=$image_name;
                }

                $insert->save();
                return redirect('/list_get');

                }
                                    
                function delete(request $request,$id){
            
                    DB::table('products')->where('id',$id)->delete();
                    return redirect('/list_get');
                }
                function edit($id){
            
                	$stocks=DB::table('products')->where('id',$id)->get();
                	return view('admin/product/edit', compact('stocks'));
            
                }
                function update(request $request,$id){
            
                     $submit= Stock::findorfail($id);
                     $submit->subcategory_id=$request->input('subcategory_id');
                     $submit->product_name=$request->input('product_name');
                     $submit->prizes=$request->input('prizes');
                     $submit->image=$request->input('image');
                     $submit->product_status=$request->input('product_status');
                     $submit->save();
                     return redirect('/list_get');
            
            
                }
}
