<?php

namespace App\Http\Controllers;

use App\Models\Logo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LogoController;

class LogoController extends Controller
{
    
    public function index()
    {
       
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

    
    public function show(Logo $logo)
    {
        //
    }

    
    public function edit(Logo $logo)
    {
        //
    }

   
    public function update(Request $request, Logo $logo)
    {
        //
    }

   
    public function destroy(Logo $logo)
    {
        //
    }
}
